<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06.02.2017
 * Time: 9:35
 */

namespace app\models;
use yii\db\ActiveRecord;

class Articles extends ActiveRecord
{
    //public $upload;
    public static function tableName(){
        return 'articles';
    }

    public function rules()
    {
        return [
                    [['title', 'preview', 'fulltxt'], 'required'],
               ];
    }


}