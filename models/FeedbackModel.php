<?php
namespace app\models;
use yii\base\Model;
use Yii;
class FeedbackModel extends Model
{
    public $name;
    public $email;
    public $subject;
    public $message;
    public $captcha;

    public function rules()
    {
        return [
            [['name','email','subject','message','captcha'],'required'],
            ['email','email'],
            ['captcha','captcha']
        ];
    }
    public function contact()
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo('mishagodovanuk@gmail.com')
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->message)
                ->send();
            return true;
        }
        return false;

    }
}