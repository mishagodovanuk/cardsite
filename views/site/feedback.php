<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\captcha\Captcha;
?>
<div class="myarticle">If you write something, soon site owner will answer to your ask.</div>
<?php if(Yii::$app->session->hasFlash('success')):?>
    <div class="alert alert-success">Your email was send</div>
<?php else:?>
<div class="row">
    <div class="col-lg-5">
<?php $form=ActiveForm::begin(['options'=>['class'=>'form-group']])?>
<?=$form->field($model,'name')->label('Name')?>
<?=$form->field($model,'email')->label('Email')?>
<?=$form->field($model,'subject')->label('Subject')?>
<?=$form->field($model,'message')->textarea()->label('Message')?>
<?= $form->field($model, 'captcha')->widget(Captcha::className(), [
    'captchaAction' => ['/site/captcha']
]) ?>
<?=Html::submitButton('Sent',['class'=>'btn btn-success'])?>
<?php ActiveForm::end()?>
    </div>
</div>
<?php endif;?>