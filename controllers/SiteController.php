<?php
namespace app\controllers;
use app\models\Articles;
use app\models\FeedbackModel;
use app\models\UploadForm;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $this->view->title = 'Misha Hodovanuk';
        return $this->render('index.twig');
    }

    public function actionInfo()
    {
        //FB::send();
        $model = Articles::find()->orderBy(['date' => SORT_DESC])->all();
        $this->view->title = 'Articles';
        return $this->render('info', ['model' => $model]);
    }

    public function actionAbout()
    {
        $this->view->title = 'About';
        return $this->render('about.twig');
    }

    public function actionFeedback()
    {
        $this->view->title = 'Feed back';
        $model = new FeedbackModel();
        if ($model->load(Yii::$app->request->post()) && $model->contact()) {
            Yii::$app->session->setFlash('feedback_success');
            return $this->render('state.twig');
        }
        return $this->render('feedback', ['model' => $model]);
    }



    public function actionArticle($id)
    {
        $model = Articles::findOne($id);
        return $this->render('article.twig', ['model' => $model]);
    }

    public function actionPortfolio()
    {
        $this->view->title = 'Portfolio';
        return $this->render('portfolio.twig');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionUpdate($id)
    {
        if(Yii::$app->user->isGuest)
        {
            return $this->redirect('login');
        }
        $model=Articles::findOne($id);
        if($model->load(Yii::$app->request->post())&&$model->save())
        {
            Yii::$app->session->setFlash('update_success');
            return $this->render('state.twig');
        }
        return $this->render('update.twig',['model' => $model]);
    }

    public function actionCreateart()
    {
        $this->view->title = 'Create article';
        $model = new Articles();
        if($model->load(Yii::$app->request->post()))
        {
            $model->image=UploadedFile::getInstance($model,'image');
            if($model->validate())
            {
                $model->image->saveAs('uploads/' . $model->image->baseName . '.' . $model->image->extension);
                if($model->save())
                {
                 Yii::$app->session->setFlash('article_created');
                 return $this->render('state.twig');
                }
            }
        }
        return $this->render('create_article.twig', ['model' =>$model]);
    }

    public function actionLogint()
    {
        return $this->render('login.twig');
    }


    public function actionArticles()
    {
        $this->view->title="Articles";
        $model = Articles::find()->orderBy(['date' => SORT_DESC])->all();
        return $this->render('articles.twig',['model' => $model]);
    }

    public function actionDelete($id)
    {
        if(Yii::$app->user->isGuest)
        {
            return $this->redirect('login');
        }
        else {

                $model = Articles::findOne($id);
                $this->view->title = "Delete " . $model->title;
                if ($model->delete()) {
                Yii::$app->session->setFlash('delete');
                return $this->render('state.twig');
                }
            }
    }

}